# GitlabSample [![build status](https://gitlab.com/parroty/gitlab_sample/badges/master/build.svg)](https://gitlab.com/parroty/gitlab_sample/commits/master) [![coverage report](https://gitlab.com/parroty/gitlab_sample/badges/master/coverage.svg)](https://gitlab.com/parroty/gitlab_sample/commits/master)

Sample elixir project to integrate gitlab ci and excoveralls

## Configuration
- Specify `mix coveralls` task in `.gitlab-ci.yml`.
- In the `CI/CD Pipelines` menu, configure `Test coverage parsing` as `\[TOTAL\]\s+(\d+\.\d+)%`.

